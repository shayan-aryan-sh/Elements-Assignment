package com.shayan.elementsassignment.di.module;

import android.content.Context;

import com.shayan.elementsassignment.data.DataRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
@Singleton
public class ServiceModule {

    private Context context;

    public ServiceModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context providesContext() {
        return context.getApplicationContext();
    }

    @Singleton
    @Provides
    public DataRepository providesDataRepository() {
        return new DataRepository();
    }
}
