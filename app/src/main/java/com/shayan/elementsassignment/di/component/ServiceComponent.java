package com.shayan.elementsassignment.di.component;

import com.shayan.elementsassignment.di.module.ServiceModule;
import com.shayan.elementsassignment.viewmodel.MainViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ServiceModule.class)
public interface ServiceComponent {

    void inject(MainViewModel mainViewModel);

}
