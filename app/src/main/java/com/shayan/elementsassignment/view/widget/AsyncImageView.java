package com.shayan.elementsassignment.view.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.shayan.elementsassignment.R;

/**
 * Created by Shayan on 12/8/2016.
 */
public class AsyncImageView extends ImageView {

    public AsyncImageView(Context context) {
        super(context);
    }

    public AsyncImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void load(String imageUrl) {
        Log.d("AsyncImageView", "Loading image: " + imageUrl);

        Glide.with(getContext())
                .load(imageUrl)
                .signature(new StringSignature(String.valueOf(System.currentTimeMillis() / (24 * 60 * 60 * 1000))))
                .placeholder(R.drawable.place_holder_rect)
                .error(R.drawable.place_holder_rect)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .crossFade()
                .into(this);
    }
}
