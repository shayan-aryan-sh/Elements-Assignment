package com.shayan.elementsassignment.view.widget;

import android.content.Context;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shayan.elementsassignment.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Setter;

/**
 * Created by Shayan on 12/08/2016.
 */
public class ErrorView extends LinearLayout {

    @Bind(R.id.error_btn)
    Button errorBtn;
    @Bind(R.id.error_message)
    TextView errorTxt;
    @Bind(R.id.error_icon)
    ImageView errorIcon;

    @Setter
    private Runnable btnAction;

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ErrorView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        View root = LayoutInflater.from(context).inflate(R.layout.layout_error, this);

        if (isInEditMode()) return;

        ButterKnife.bind(this, root);
    }

    @OnClick(R.id.error_btn)
    public void onClickBtn() {
        if (btnAction != null)
            btnAction.run();
    }

    public void setErrorText(String txt) {
        errorTxt.setText(txt);
    }

    public void setErrorText(@StringRes int txtRes) {
        errorTxt.setText(txtRes);
    }

    public void hide() {
        setVisibility(INVISIBLE);
    }

    public void show() {
        setVisibility(VISIBLE);
    }
}
