package com.shayan.elementsassignment.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.shayan.elementsassignment.R;
import com.shayan.elementsassignment.model.Post;
import com.shayan.elementsassignment.view.activity.base.BaseActivity;
import com.shayan.elementsassignment.view.widget.AsyncImageView;

import butterknife.Bind;

/**
 * Created by Shayan on 12/8/2016.
 */
public class DetailsActivity extends BaseActivity {
    
    private static final String EXTRA_POST = "post";

    private Post post;

    @Bind(R.id.post_collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @Bind(R.id.post_image)
    AsyncImageView image;
    @Bind(R.id.post_title)
    TextView title;
    @Bind(R.id.post_description)
    TextView description;

    public static Intent createIntent(Context context, Post post) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(EXTRA_POST, post);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        post = (Post) getIntent().getSerializableExtra(EXTRA_POST);

        initViews();
    }

    private void initViews() {
        init();
        enableBackButton();
        collapsingToolbarLayout.setTitle(post.getTitle());
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(this, android.R.color.transparent));

        if (!TextUtils.isEmpty(post.getTitle())) {
           title.setText(Html.fromHtml(post.getTitle()));
        } else {
            title.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(post.getDescription())) {
            description.setText(Html.fromHtml(post.getDescription()));
        } else {
            description.setVisibility(View.GONE);
        }

        image.load(post.getImageUrl());
    }
}
