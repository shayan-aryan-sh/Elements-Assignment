package com.shayan.elementsassignment.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import com.shayan.elementsassignment.R;
import com.shayan.elementsassignment.databinding.ActivityMainBinding;
import com.shayan.elementsassignment.model.Post;
import com.shayan.elementsassignment.view.activity.base.BaseActivity;
import com.shayan.elementsassignment.view.adapter.PostsAdapter;
import com.shayan.elementsassignment.view.widget.ErrorView;
import com.shayan.elementsassignment.viewmodel.MainViewModel;

import java.util.List;

import butterknife.Bind;

public class MainActivity extends BaseActivity implements MainViewModel.DataListener, AdapterView.OnItemClickListener {

    private ActivityMainBinding binding;
    private MainViewModel viewModel;

    private PostsAdapter adapter;

    @Bind(R.id.posts_list)
    RecyclerView recyclerView;
    @Bind(R.id.posts_progress)
    View progressView;
    @Bind(R.id.posts_errorView)
    ErrorView errorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initViews();

        viewModel = new MainViewModel(this, this, lifecycleSubject);
        binding.setViewModel(viewModel);
        fetchData();
    }

    private void fetchData() {
        viewModel.fetchData();
    }

    private void initViews() {
        init();
        setToolbarTitle(R.string.app_name);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new PostsAdapter(this, this);
        recyclerView.setAdapter(adapter);

        errorView.setBtnAction(() -> {
            errorView.hide();
            showProgress();
            fetchData();
        });
    }

    @Override
    public void onData(List<Post> posts) {
        hideProgress();
        adapter.setItems(posts);
    }

    @Override
    public void failure(@StringRes int messageRes) {
        hideProgress();
        errorView.setErrorText(messageRes);
        errorView.show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
        Post item = adapter.getItem(pos);
        startActivity(DetailsActivity.createIntent(this, item));
    }

    private void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressView.setVisibility(View.INVISIBLE);
    }
}
