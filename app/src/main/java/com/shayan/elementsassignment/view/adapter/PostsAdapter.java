package com.shayan.elementsassignment.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.shayan.elementsassignment.R;
import com.shayan.elementsassignment.model.Post;
import com.shayan.elementsassignment.view.widget.AsyncImageView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Shayan on 12/8/2016.
 */
public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    private AdapterView.OnItemClickListener onItemClickListener;
    private LayoutInflater inflater;

    private List<Post> items;

    public PostsAdapter(Context context, AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row_post, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post item = getItem(position);

        if (!TextUtils.isEmpty(item.getTitle())) {
            holder.title.setText(Html.fromHtml(item.getTitle()));
            holder.title.setVisibility(View.VISIBLE);
        } else {
            holder.title.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(item.getDescription())) {
            holder.description.setText(Html.fromHtml(item.getDescription()));
            holder.description.setVisibility(View.VISIBLE);
        } else {
            holder.description.setVisibility(View.GONE);
        }

        holder.image.load(item.getImageUrl());
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public Post getItem(int position) {
        return items.get(position);
    }

    public void setItems(List<Post> devices) {
        items = devices;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.post_image)
        AsyncImageView image;
        @Bind(R.id.post_title)
        TextView title;
        @Bind(R.id.post_description)
        TextView description;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener((view)->
                    onItemClickListener.onItemClick(null, view, getAdapterPosition(), view.getId())
            );
        }
    }
}
