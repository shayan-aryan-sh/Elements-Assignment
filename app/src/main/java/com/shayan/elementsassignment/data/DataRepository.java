package com.shayan.elementsassignment.data;

import android.os.AsyncTask;

import com.shayan.elementsassignment.model.Post;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.BehaviorSubject;

/**
 * Created by Shayan on 12/8/2016.
 */
public class DataRepository {

    private static final String CSV_URL = "https://docs.google.com/spreadsheet/ccc?key=0Aqg9JQbnOwBwdEZFN2JKeldGZGFzUWVrNDBsczZxLUE&single=true&gid=0&output=csv";

    public DataRepository() {
    }

    public Observable<List<Post>> getPosts() {
        BehaviorSubject<List<Post>> subject = BehaviorSubject.create();

        new AsyncTask<Void, Void, List<Post>>() {

            @Override
            protected List<Post> doInBackground(Void... voids) {
                try {
                    //download and parse the CSV file using apache commons csv
                    URL url = new URL(CSV_URL);
                    CSVParser parser = CSVParser.parse(url, Charset.defaultCharset(), CSVFormat.RFC4180);
                    List<Post> posts = new ArrayList<>();
                    int i = 0;
                    for (CSVRecord csvRecord : parser) {
                        //skip the first row
                        if (i > 0) {
                            //parse the row
                            posts.add(new Post(csvRecord.get(0), csvRecord.get(1), csvRecord.get(2)));
                        } else {
                            i++;
                        }
                    }
                    return posts;
                } catch (Exception e) {
                    e.printStackTrace();
                    subject.onError(e);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(List<Post> posts) {
                if (posts != null && posts.size() > 0)
                    subject.onNext(posts);
            }
        }.execute();

        return subject;
    }
}
