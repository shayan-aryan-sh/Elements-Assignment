package com.shayan.elementsassignment.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Shayan on 12/8/2016.
 */
@AllArgsConstructor(suppressConstructorProperties = true)
@Getter
@Setter
public class Post implements Serializable{
    private String title;
    private String description;
    private String imageUrl;
}
