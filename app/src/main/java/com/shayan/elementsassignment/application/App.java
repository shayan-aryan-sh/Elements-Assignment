package com.shayan.elementsassignment.application;

import android.app.Application;
import android.content.Context;

import com.shayan.elementsassignment.di.component.DaggerServiceComponent;
import com.shayan.elementsassignment.di.component.ServiceComponent;
import com.shayan.elementsassignment.di.module.ServiceModule;

import lombok.Getter;
import lombok.Setter;

public class App extends Application {

    @Getter
    @Setter //for Automated Tests
    private ServiceComponent serviceComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    private void initDagger() {
        serviceComponent = DaggerServiceComponent.builder()
                .serviceModule(new ServiceModule(this))
                .build();
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

}