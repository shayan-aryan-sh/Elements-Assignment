package com.shayan.elementsassignment.viewmodel;

import android.content.Context;
import android.support.annotation.StringRes;

import com.shayan.elementsassignment.R;
import com.shayan.elementsassignment.application.App;
import com.shayan.elementsassignment.data.DataRepository;
import com.shayan.elementsassignment.model.Post;
import com.trello.rxlifecycle.ActivityEvent;
import com.trello.rxlifecycle.RxLifecycle;

import java.net.UnknownHostException;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Shayan on 12/8/2016.
 */
public class MainViewModel implements ViewModel {

    @Inject
    DataRepository dataRepository;

    private Observable<ActivityEvent> lifecycleSubject;
    private DataListener listener;

    public MainViewModel(Context context, DataListener listener, Observable<ActivityEvent> lifecycleSubject) {
        this.lifecycleSubject = lifecycleSubject;
        this.listener = listener;
        App.get(context).getServiceComponent().inject(this);
    }

    public void fetchData() {
        dataRepository.getPosts()
                .compose(RxLifecycle.bindUntilActivityEvent(lifecycleSubject, ActivityEvent.DESTROY))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(listener::onData,
                        throwable -> {
                            throwable.printStackTrace();
                            int messageRes = throwable instanceof UnknownHostException ? R.string.no_internet : R.string.something_is_wrong;
                            listener.failure(messageRes);
                        });
    }

    @Override
    public void onDestroy() {
        lifecycleSubject = null;
        listener = null;
    }

    public interface DataListener {
        void onData(List<Post> posts);

        void failure(@StringRes int messageRes);
    }
}
