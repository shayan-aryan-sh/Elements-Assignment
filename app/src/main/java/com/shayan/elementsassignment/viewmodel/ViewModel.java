package com.shayan.elementsassignment.viewmodel;

/**
 * Created by Shayan on 12/8/2016.
 */
public interface ViewModel {
    void onDestroy();
}
